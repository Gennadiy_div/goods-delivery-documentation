Главная страница
=================

Запрос для получения данных главной страницы
---------------------------------------------

.. http:get:: /main-page

   **Пример запроса**:

   .. sourcecode:: http

      GET /main-page HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "categories": [
        {
         "id": 8,
         "title": "Хлебобулочные изделия",
         "thumbnail_url": "http://goods-delivery-backend.test/storage/categories/sZxsECLs4ZYR6k6WgfNXNU6Oa3aFrHJX4LsVm7hj.png",
         "products_count": 3
        }
       ],
       "discountProducts": [
        {
         "id": 7,
         "title": "Заводской 1",
         "thumbnail_url": "http://goods-delivery-backend.test/storage/products/Vs03fxhb61wqpEPAXNj4JaurgKD6uFxyH7ZsLeAY.png",
         "weight": 300,
         "price": 400,
         "discount_price": 200,
         "discount_percent": 50
        }
       ]
      }

   :>json categories: Массив состоящий из объектов :ref:`Категории <category>`
   :>json discountProducts: Массив состоящий из объектов :ref:`Товаров (сокращенный) <small product>`