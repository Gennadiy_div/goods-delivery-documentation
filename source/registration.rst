Регистрация
===========

Ожидаемая схема регистрации:

1. Отправляется запрос на проверку наличия клиента (запрос 3.1)
2. Если клиент существует в нашей базе

   1. Отправляется запрос на получение полной информации о клиенте (запрос 3.2)
   2. После отправляется запрос на регистрацию пользователя(запрос 3.4)

3. Если клиент не существует в нашей базе :

   1. Отправляется запрос на регистрацию клиента и пользователя (запрос 3.3)

4. После отправки запросов на регистрация (запросы 3.3 и 3.4), отправляется запрос на отправку кода подтверждение по СМС (запрос 3.5)
5. После получение СМС кода отправляется запрос на подтверждение регистрации(запрос 3.6)


Запрос на проверку наличия клиентов
--------------------------------------

.. http:get:: /clients/check-for-existing

	Запрос делается для проверки существование клиента в базе веб-сервиса

   **Пример запроса**:

   .. sourcecode:: http

      GET /clients/check-for-existing?identification_number=123412341234 HTTP/1.1
      Accept: application/json

   **Пример ответа №1**:

   Когда клиент существует в базе

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
        "status": "exists",
        "client": {
          "id": 1,
          "title": "Магнум",
          "identification_number": "123412341234",
          "face_type": "legal",
          "full_title": "ТОО \"Магнум\"",
          "is_approved": true
        }
      }

   **Пример ответа №2**:

   Когда клиент не существует в базе

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
        "status": "not-exists",
      }

   .. important::
      В данном запросе стоит ограничение на количество отправок. Максимальное количество запросов в минуту **10**. Это сделано для того чтобы избежать получения списка всех клиентов третьим лицом путем перебора БИН/ИИН.

   :query identification_number: (*required*) БИН/ИИН клиента

   :>json status: Одно из *exists, not-exists*
   :>json client: Будет заполнен если status равно exists. :ref:`Сокращенный объект Client <small client object>`

Запрос на получение полной информации клиента
-----------------------------------------------

.. http:get:: /clients/( int:client_id)

	Запрос возвращает полную информацию клиента с ID равно `client_id`

   **Пример запроса**:

   .. sourcecode:: http

      GET /clients/1 HTTP/1.1
      Accept: application/json

   **Пример ответа №1**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "client": {
            "id": 1,
            "identification_number": "123412341234",
            "face_type": "legal",
            "full_title": "ТОО \"Магнум\"",
            "is_approved": true,
            "stores": [
               {
                  "id": 1,
                  "title": "Основная точка",
                  "segment": "Супермаркет",
                  "opening_time": "11:18:00",
                  "delivery_time": "11:18:00",
                  "non_working_days": [1, 2],
                  "address": {
                     "name": "Основная точка",
                     "address": "1-й микрорайон, 76",
                     "lat": "43.23106699961803",
                     "long": "76.843884"
                  }
               }
            ]
         }
      }

   **Пример ответа №2**:

   В случае отсутствие клиента с ID равно `client_id`

   .. sourcecode:: http

      HTTP/1.1 400 Bad request
      Content-Type: application/json

      {
         "key": "model_not_found",
         "message": "Запись не найдена",
         "data": {
          "model": "App\\Models\\Client",
          "id": "12"
         }
      }

   .. important::
      В данном запросе стоит ограничение на количество отправок. Количество запросов в минуту **10**. Это сделано для того чтобы избежать получения списка всех клиентов третьим лицом путем перебора ID.

   :>json client: :ref:`объект Client <client object>`

   :statuscode 200: Успешный ответ

   :statuscode 400: Клиент не найден

Запрос на регистрацию клиента, торговой точки и пользователя
-------------------------------------------------------------

.. http:post:: /register/client-and-user

   **Пример запроса**:

   .. sourcecode:: http

      POST /register/client-and-user HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "client": {
        "title": "Айгерим",
        "identification_number": "123412341237",
        "face_type": "legal"
       },
       "store": {
        "address": {
         "name": "Тест",
         "address": "Тест",
         "lat": "50.00",
         "long": "35.00"
        },
        "segment": "Магазин",
        "opening_time": "09:00",
        "delivery_time": "12:00",
        "non_working_days": [5, 6]
       },
       "user": {
        "full_name": "Оразов Азамат",
        "phone": "77770001122",
        "position": "owner"
       }
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "token": "dVd708amz6bnWgM3h9qU4S0w98CPmEHZ73wWZOcH9TEm4fADiD"
      }

   :<json client: *(object, required)* Объект содержащий информацию о клиенте
   :<json client.title: *(string, required)* Название клиента
   :<json client.identification_number: *(string, required)* БИН/ИИН клиента
   :<json client.face_type: *(string, required)* Тип клиента. Одно из *legal (юр лицо), natural (физ лицо)*
   :<json store: *(object, required)* Объект содержащий информацию о торговой точке
   :<json store.segment: *(string)* Сегмент
   :<json store.opening_time: *(time)* Время открытия
   :<json store.delivery_time: *(time)* Время открытия
   :<json store.non_working_days: *(array)* Не рабочие дни в формате [0-6]
   :<json store.address: *(object, required)* Объект содержащий информацию об адресе
   :<json store.address.name: *(string, required)* Адрес
   :<json store.address.address: *(string, required)* Адрес
   :<json store.address.lat: *(string, required)* Широта
   :<json store.address.long: *(string, required)* Долгота
   :<json user: *(object, required)* Объект содержащий информацию о пользователе
   :<json user.full_name: *(string, required)* ФИО пользователя
   :<json user.phone: *(string, required)* Номер телефона пользователя
   :<json user.position: *(string, required)* Должность пользователя. Одно из *owner, supervisor, seller*

   :>json token: *(string 50 chars)* Токен регистрации по которому проводится подтверждение регистрации

   :statuscode 200: Успешный ответ

   :statuscode 422: Ошибка валидации

Запрос на регистрацию пользователя
----------------------------------

.. http:post:: /register/user

   **Пример запроса**:

   .. sourcecode:: http

      POST /register/client-and-user HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "client_id": 1,
       "store_id": 1,
       "user": {
        "full_name": "Оразов Азамат",
        "phone": "77770001122",
        "position": "owner"
       }
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "token": "dVd708amz6bnWgM3h9qU4S0w98CPmEHZ73wWZOcH9TEm4fADiD"
      }

   :<json client_id: *(int, required)* ID клиента в веб-сервисе
   :<json store_id: *(ind, required)* ID торговой точки в веб-сервисе
   :<json user: *(object, required)* Объект содержащий информацию о пользователе
   :<json user.full_name: *(string, required)* ФИО пользователя
   :<json user.phone: *(string, required)* Номер телефона пользователя
   :<json user.position: *(string, required)* Должность пользователя. Одно из *owner, supervisor, seller*

   :>json token: *(string 50 chars)* Токен регистрации по которому проводится подтверждение регистрации

   :statuscode 200: Успешный ответ

   :statuscode 422: Ошибка валидации

Отправка кода подтверждения по СМС
----------------------------------

   **Пример запроса**:

   .. sourcecode:: http

      POST /register/send-code HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "token": "dVd708amz6bnWgM3h9qU4S0w98CPmEHZ73wWZOcH9TEm4fADiD"
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {}

Подтверждение регистрации
-------------------------

.. http:post:: /register/complete

   **Пример запроса**:

   .. sourcecode:: http

      POST /register/complete HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "token": "dVd708amz6bnWgM3h9qU4S0w98CPmEHZ73wWZOcH9TEm4fADiD",
       "code": 1111
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "access_token": "12|nd0COYPonQ6hG76AF6dztCDaQIiwjAf61msHOtTY",
         "token_type": "Bearer",
         "user": {
          "id":5,
          "client_id": 1,
          "store_id": 1,
          "full_name": "Тестов Тест",
          "phone": "77771112233",
          "position": "owner",
         }
      }

   :>json access_token: *(string)* Токен аутентификации
   :>json token_type: *(string)* Тип токена
   :>json user: *(object)* :ref:`Объект Пользователя <user>`

   :statuscode 200: Успешный ответ

   :statuscode 400: Ошибка при подтверждении