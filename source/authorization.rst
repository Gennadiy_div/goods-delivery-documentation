Авторизация
===========

Ожидаемая схема авторизации:

1. Отправляется запрос на отправку кода подтверждение по СМС (запрос 4.1)
2. После получение кода подтверждение отправляется запрос на подтверждение (запрос 4.2).
3. После подтверждение выдается токен, который нужно добавить в заголовок Authorization с типом Bearer в последующих запросах
4. В случае необходимости переотправки СМС, используется запрос 4.3

Запрос на отправку кода подтверждение по СМС
--------------------------------------------

.. http:post:: /auth/send-code

   **Пример запроса**:

   .. sourcecode:: http

      POST /auth/send-code HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "phone": "77012223344"
      }

   **Пример ответа №1**:

   Когда пользователь существует в базе

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
        "token": "zlrXtUDjo8LA1OMhSSGOgrJZN4Li1U"
      }

   **Пример ответа №2**:

   Когда пользователь не существует в базе

   .. sourcecode:: http

      HTTP/1.1 400 Bad request
      Content-Type: application/json

      {
       "key": "user_not_found",
       "message": "Номер не зарегистрирован",
       "data": null
      }

   :<json phone: (*required*) Номер телефона пользователя

   :>json token: Токен по которому идет подтверждение кода

   :statuscode 200: Успешный ответ

   :statuscode 400: Ошибка

Запрос на подтверждение кода
----------------------------

.. http:post:: /auth

   **Пример запроса**:

   .. sourcecode:: http

      POST /auth/send-code HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "token": "zlrXtUDjo8LA1OMhSSGOgrJZN4Li1U",
       "code": "1111"
      }

   **Пример ответа №1**:

   Когда код верный

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "access_token": "14|4XZlSMHHIcGpURzpwV08bWP7aZC0TqNvxi1KU8Lw",
       "token_type": "Bearer",
       "user": {
        "id": 3,
        "client_id": 15,
        "client_store_id": 5,
        "full_name": "Testov Test",
        "phone": "77770001122",
        "position": "owner",
       }
      }

   **Пример ответа №2**:

   Когда код неверный

   .. sourcecode:: http

      HTTP/1.1 400 Bad request
      Content-Type: application/json

      {
       "key": "invalid_code",
       "message": "Неверный код",
       "data": null
      }

   :<json token: (*required*) Токен полученный с запроса 4.1
   :<json code: (*required*) Код полученный по СМС

   :>json access_token: Токен по которому идет авторизация в последующих запросах
   :>json token_type: Тип токена
   :>json user: Объект :ref:`User <user>`

   :statuscode 200: Успешный ответ

   :statuscode 400: Ошибка


Переотправка кода
-----------------

.. http:post:: /auth/resend-code

   **Пример запроса**:

   .. sourcecode:: http

      POST /auth/resend-code HTTP/1.1
      Accept: application/json

   .. sourcecode:: json

      {
       "token": "zlrXtUDjo8LA1OMhSSGOgrJZN4Li1U"
      }

   :<json token: (*required*) Токен полученный с запроса 4.1

   :statuscode 200: Успешный ответ

   :statuscode 400: Ошибка
   :statuscode 429: Превышение лимита