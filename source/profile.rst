Профиль
=======

Запрос на получение данных пользователя
---------------------------------------

.. important::

   Данный запрос доступен только авторизованным пользователям

.. http:get:: /profile

   **Пример запроса**:

   .. sourcecode:: http

      GET /profile HTTP/1.1
      Accept: application/json
      Authorization: Bearer sdfni234dfb3fgdsfg43vd

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "user": {
        "id": 3,
        "client_id": 15,
        "client_store_id": 5,
        "full_name": "Testov Test",
        "phone": "77770001122",
        "position": "owner",
       },
       "client": {
        "id": 15,
        "title": "Hleb and sol'",
        "identification_number": "123412341230",
        "face_type": "legal",
        "full_title": "ТОО \"Hleb and sol'\"",
        "is_approved": false
       },
       "store": {
        "id": 5,
        "title": "Основная точка",
        "segment": "test",
        "opening_time": "09:00:00",
        "delivery_time": "11:00:00",
        "non_working_days": [5, 6],
        "address": {
         "name": "test",
         "address": "test",
         "lat": "test",
         "long": "test"
        }
       }
      }

   :>json user: Объект :ref:`Пользователь <user>`
   :>json client: :ref:`Объект Клиент <client object>`
   :>json store: :ref:`Объект Торговая точка <store>`

Выход из системы
-----------------

.. important::

   Данный запрос доступен только авторизованным пользователям

.. http:post:: /logout

   **Пример запроса**:

   .. sourcecode:: http

      POST /logout HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json
