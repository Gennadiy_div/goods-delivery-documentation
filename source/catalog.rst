Каталог
=======

Запрос на получение всего каталога
----------------------------------

.. http:get:: /catalog

   **Пример запроса**:

   .. sourcecode:: http

      GET /catalog HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "sections": [
        {
         "id": 1,
         "title": "Хлебобулочные изделия",
         "categories": [
           {
             "id": 8,
             "title": "Хлеб",
             "products": [
              {
               "id": 7,
               "title": "Заводской",
               "thumbnail_url": "http://goods-delivery-backend.test/storage/products/Vs03fxhb61wqpEPAXNj4JaurgKD6uFxyH7ZsLeAY.png",
               "weight": 300,
               "price": 400,
               "discount_price": 200,
               "discount_percent": 50
              }
             ]
           }
         ]
        },
       ]
      }

   :>json sections: Объект :ref:`Группы Категорий <section>`
   :>json sections.*.categories.*: :ref:`Объект Категории <category>`
   :>json sections.*.categories.*.product.*: :ref:`Объект Товара <small product>`

Поиск по каталогу
----------------------------------

.. http:get:: /catalog/search

   **Пример запроса**:

   .. sourcecode:: http

      GET /catalog/search?search=Хле HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "categories": [
        {
         "id": 1,
         "title": "Хлеб",
        },
       ],
       "groups": [
        {
         "category": {
          "id": 1,
          "title": "Хлеб"
         },
         "products": [
          {
            "id": 7,
            "title": "Заводской хлеб",
            "thumbnail_url": "http://goods-delivery-backend.test/storage/products/Vs03fxhb61wqpEPAXNj4JaurgKD6uFxyH7ZsLeAY.png",
            "weight": 300,
            "price": 400,
            "discount_price": 200,
            "discount_percent": 50,
          }
         ]
        }
       ]
      }

   :query search: (*required*) Строка поиска

   :>json categories: Список :ref:`категорий <category>` подходящих по поиску.
   :>json groups: Сгруппированный список товаров по категории подходящих по поиску
   :>json groups.*.category: Категория группы
   :>json groups.*.products: Список товаров


Детальная информация по товару
------------------------------

.. http:get:: /products/( int:product_id)

	Запрос возвращает полную информацию товара с ID равно `product_id`

   **Пример запроса**:

   .. sourcecode:: http

      GET /products/1 HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
       "data": {
        "id": 1,
        "title": "Батон",
        "thumbnail_url": "http://goods-delivery-backend.test/storage/products/Vs03fxhb61wqpEPAXNj4JaurgKD6uFxyH7ZsLeAY.png",
        "composition": "Мука\соль\дрожь",
        "expiration_days": 3,
        "weight": 300,
        "price": 400,
        "discount_price": 200,
        "discount_percent": 50
       }
      }

   :>json data: Объект :ref:`товара <product>`.

